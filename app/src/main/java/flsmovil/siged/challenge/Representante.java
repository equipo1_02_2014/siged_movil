package flsmovil.siged.challenge;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.astuetz.PagerSlidingTabStrip;

import flsmovil.siged.challenge.Comun.DatosUsuario;
import flsmovil.siged.challenge.Comun.TabColor;
import flsmovil.siged.challenge.fragmentos.Representante_Fragment;
import flsmovil.siged.challenge.fragmentos.noticias.Noticias_Fragment;
import flsmovil.siged.challenge.fragmentos.posiciones.Posiciones_Fragment;
import flsmovil.siged.challenge.fragmentos.resultados.Resultados_Fragment;


public class Representante extends FragmentActivity {

    static final String APP_NAME = "Fundación Luis Sojo";

    private PagerSlidingTabStrip tabs;
    private ViewPager pager;
    private MyPagerAdapter adapter;

    private String[] mFragmentsTitles;
    private DrawerLayout mDrawerLayout;
    private LinearLayout mDrawerContainer;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mTitle;
    private CharSequence mDrawerTitle;

    private int currentColor = 0xFFC74B46; // for tab selector

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getActionBar().setHomeButtonEnabled(true);

        final DatosUsuario datos = (DatosUsuario)getApplicationContext();
        datos.ConfigurarUsuario(this);

        mFragmentsTitles = getResources().getStringArray(R.array.fragments_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerContainer = (LinearLayout) findViewById(R.id.drawer_container);
        mDrawerList = (ListView) findViewById(R.id.drawer_list);
        // Set the adapter for the list view
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, mFragmentsTitles));
        // Set the list's click listener
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        mTitle = mDrawerTitle = APP_NAME;

        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getActionBar().setTitle(mTitle);

            /*    if(pager.getCurrentItem()==0)
                {
                    HideActionBar();
                } */
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActionBar().setTitle(APP_NAME);

        /*        if(pager.getCurrentItem()==0)
                {
                    ShowActionBar();
                }*/
            }
        };

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        pager = (ViewPager) findViewById(R.id.pager);
        adapter = new MyPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        tabs.setViewPager(pager);
        tabs.setOnPageChangeListener(page_listener);
        pager.setCurrentItem(0);
        changeColor(0); // Default color

        HideActionBar();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.session_icon:
                Intent login = new Intent(this,
                        Login.class);
                startActivity(login);
            /*    if (!mDrawerLayout.isDrawerOpen(mDrawerContainer)) {
                    mDrawerLayout.openDrawer(mDrawerContainer);
                }
                else{
                    mDrawerLayout.closeDrawer(mDrawerContainer);
                }*/
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void changeColor(int newColor) {

        tabs.setIndicatorColor(newColor);
        currentColor = newColor;

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("currentColor", currentColor);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        currentColor = savedInstanceState.getInt("currentColor");
    }

    private ViewPager.OnPageChangeListener page_listener = new ViewPager.OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {

            changeColor(TabColor.getColor(position));

            setTitle(mFragmentsTitles[position]);

            if (position == 0) HideActionBar();
            else ShowActionBar();
        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    };

    public class MyPagerAdapter extends FragmentStatePagerAdapter {

        public MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentsTitles[position];
        }

        @Override
        public int getCount() {
            return mFragmentsTitles.length;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 0:
                    return new Representante_Fragment();
                case 1:
                    return new Noticias_Fragment(); // Noticias
                case 2:
                    return new Resultados_Fragment();
                case 3:
                    return new Posiciones_Fragment();

                default:
                    return new Representante_Fragment();
            }
        }

    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
            selectItem(position);

            if (position == 0) HideActionBar();
            else ShowActionBar();
        }
    }

    /**
     * Swaps fragments in the main content view
     */
    private void selectItem(int position) {
        // Create a new fragment and specify the planet to show based on position

        if (position + 1 >= mFragmentsTitles.length) {
            Intent reposo = new Intent(this,
                    Reposo.class);
            startActivity(reposo);
        } else {
            pager.setCurrentItem(position);
        }

        // Highlight the selected item, update the title, and close the drawer
        mDrawerList.setItemChecked(position, true);
        setTitle(mFragmentsTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerContainer);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerContainer);
        menu.findItem(R.id.session_icon).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    void HideActionBar() {
        getActionBar().hide();
        tabs.setVisibility(View.GONE);
    }

    void ShowActionBar() {
        getActionBar().show();
        tabs.setVisibility(View.VISIBLE);
    }

    public void ButtonClick(View v)
    {
        switch (v.getId())
        {
            case (R.id.btnNoticias):
                pager.setCurrentItem(1);
                break;
            case (R.id.btnResultados):
                pager.setCurrentItem(2);
                break;
            case (R.id.btnPosiciones):
                pager.setCurrentItem(3);
                break;
        }
    }
}
