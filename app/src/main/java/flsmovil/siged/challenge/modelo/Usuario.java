package flsmovil.siged.challenge.modelo;

/**
 * Created by ECarrera on 27/11/14.
 */
public class Usuario {

    Integer id;
    String descripcion;
    String photoBlob;
    Integer photoContentLength;
    String photoContentType;

    String nombreusuario;
    String password;
    String nombrerol;

    public Usuario(Integer id, String descripcion, String photoBlob, Integer photoContentLength, String photoContentType, String nombreusuario, String password, String nombrerol) {
        this.id = id;
        this.descripcion = descripcion;
        this.photoBlob = photoBlob;
        this.photoContentLength = photoContentLength;
        this.photoContentType = photoContentType;
        this.nombreusuario = nombreusuario;
        this.password = password;
        this.nombrerol = nombrerol;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getPhotoBlob() {
        return photoBlob;
    }

    public void setPhotoBlob(String photoBlob) {
        this.photoBlob = photoBlob;
    }

    public Integer getPhotoContentLength() {
        return photoContentLength;
    }

    public void setPhotoContentLength(Integer photoContentLength) {
        this.photoContentLength = photoContentLength;
    }

    public String getPhotoContentType() {
        return photoContentType;
    }

    public void setPhotoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
    }

    public String getNombreusuario() {
        return nombreusuario;
    }

    public void setNombreusuario(String nombreusuario) {
        this.nombreusuario = nombreusuario;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNombrerol() {
        return nombrerol;
    }

    public void setNombrerol(String nombrerol) {
        this.nombrerol = nombrerol;
    }
}
