package flsmovil.siged.challenge.modelo;

/**
 * Created by ECarrera on 30/11/14.
 */
public class Rango {
    Integer id;
    String descripcion;
    String observacion;
    Integer estatus;

    public Rango() {
    }

    public Rango(Integer id, String descripcion, String observacion, Integer estatus) {
        this.id = id;
        this.descripcion = descripcion;
        this.observacion = observacion;
        this.estatus = estatus;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }
}
