package flsmovil.siged.challenge.modelo;

/**
 * Created by ECarrera on 30/11/14.
 */
public class Equipo {

    Integer id;
    Rango rango;
    Categoria categoria;
    TipoEquipo tipoEquipo;
    Integer cuposTotales;
    String nombre;
    String fechaCreacion;
    // practicas
    // horarioPracticas
    // delegadoEquipos
    // atletaEquipos
    // equipoTecnicos
    // fechaCierre
    // estatus
    // fechaCierre

    public Equipo() {
    }

    public Equipo(Integer id, Rango rango, Categoria categoria, TipoEquipo tipoEquipo, Integer cuposTotales, String nombre, String fechaCreacion) {
        this.id = id;
        this.rango = rango;
        this.categoria = categoria;
        this.tipoEquipo = tipoEquipo;
        this.cuposTotales = cuposTotales;
        this.nombre = nombre;
        this.fechaCreacion = fechaCreacion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Rango getRango() {
        return rango;
    }

    public void setRango(Rango rango) {
        this.rango = rango;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public TipoEquipo getTipoEquipo() {
        return tipoEquipo;
    }

    public void setTipoEquipo(TipoEquipo tipoEquipo) {
        this.tipoEquipo = tipoEquipo;
    }

    public Integer getCuposTotales() {
        return cuposTotales;
    }

    public void setCuposTotales(Integer cuposTotales) {
        this.cuposTotales = cuposTotales;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(String fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }
}
