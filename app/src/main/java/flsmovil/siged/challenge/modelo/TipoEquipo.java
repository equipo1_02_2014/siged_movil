package flsmovil.siged.challenge.modelo;

/**
 * Created by ECarrera on 30/11/14.
 */
public class TipoEquipo {
    Integer id;
    String descripcion;
    Integer estatus;

    public TipoEquipo() {
    }

    public TipoEquipo(Integer id, String descripcion, Integer estatus) {
        this.id = id;
        this.descripcion = descripcion;
        this.estatus = estatus;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }
}
