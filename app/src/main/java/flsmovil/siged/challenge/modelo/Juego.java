package flsmovil.siged.challenge.modelo;

/**
 * Created by ECarrera on 30/11/14.
 */
public class Juego {

    Integer id;
    Equipo equipo;
    Anotador anotador;
    String escuelaContraria;
    String equipoContrario;
    String fecha;
    Integer carrerasFavor;
    Integer carrerasContra;
    String horaInicio;
    String observacion;
    String lugar;
    Integer inningsJugados;
    // evaluacionActividad
    // idCompetencia
    // stadium

    public Juego() {
    }

    public Juego(Integer id, Equipo equipo, Anotador anotador, String escuelaContraria, String equipoContrario, String fecha, Integer carrerasFavor, Integer carrerasContra, String horaInicio, String observacion, String lugar, Integer inningsJugados) {
        this.id = id;
        this.equipo = equipo;
        this.anotador = anotador;
        this.escuelaContraria = escuelaContraria;
        this.equipoContrario = equipoContrario;
        this.fecha = fecha;
        this.carrerasFavor = carrerasFavor;
        this.carrerasContra = carrerasContra;
        this.horaInicio = horaInicio;
        this.observacion = observacion;
        this.lugar = lugar;
        this.inningsJugados = inningsJugados;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Equipo getEquipo() {
        return equipo;
    }

    public void setEquipo(Equipo equipo) {
        this.equipo = equipo;
    }

    public Anotador getAnotador() {
        return anotador;
    }

    public void setAnotador(Anotador anotador) {
        this.anotador = anotador;
    }

    public String getEscuelaContraria() {
        return escuelaContraria;
    }

    public void setEscuelaContraria(String escuelaContraria) {
        this.escuelaContraria = escuelaContraria;
    }

    public String getEquipoContrario() {
        return equipoContrario;
    }

    public void setEquipoContrario(String equipoContrario) {
        this.equipoContrario = equipoContrario;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Integer getCarrerasFavor() {
        return carrerasFavor;
    }

    public void setCarrerasFavor(Integer carrerasFavor) {
        this.carrerasFavor = carrerasFavor;
    }

    public Integer getCarrerasContra() {
        return carrerasContra;
    }

    public void setCarrerasContra(Integer carrerasContra) {
        this.carrerasContra = carrerasContra;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public Integer getInningsJugados() {
        return inningsJugados;
    }

    public void setInningsJugados(Integer inningsJugados) {
        this.inningsJugados = inningsJugados;
    }
}
