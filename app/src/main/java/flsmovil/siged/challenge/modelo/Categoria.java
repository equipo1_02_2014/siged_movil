package flsmovil.siged.challenge.modelo;

/**
 * Created by ECarrera on 30/11/14.
 */
public class Categoria {
    Integer id;
    String nombre;
    Integer edadMinima;
    Integer edadMaxima;

    public Categoria() {
    }

    public Categoria(Integer id, String nombre, Integer edadMinima, Integer edadMaxima) {
        this.id = id;
        this.nombre = nombre;
        this.edadMinima = edadMinima;
        this.edadMaxima = edadMaxima;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getEdadMinima() {
        return edadMinima;
    }

    public void setEdadMinima(Integer edadMinima) {
        this.edadMinima = edadMinima;
    }

    public Integer getEdadMaxima() {
        return edadMaxima;
    }

    public void setEdadMaxima(Integer edadMaxima) {
        this.edadMaxima = edadMaxima;
    }
}
