package flsmovil.siged.challenge.modelo;

/**
 * Created by ECarrera on 30/11/14.
 */
public class Anotador {

    Integer id;
    String cedula;
    String nombre;
    String apellido;
    String telefono;
    String celular;
    String direccion;
    Long fechaNacimiento;
    String lugarNacimiento;
    String email;
    String foto;
    String nombreFoto;
    Long tamanoFoto;
    String tipoFoto;
    String fechaRegistro;
    Integer estatus;

    public Anotador() {
    }

    public Anotador(Integer id, String cedula, String nombre, String apellido, String telefono, String celular, String direccion, Long fechaNacimiento, String lugarNacimiento, String email, String foto, String nombreFoto, Long tamanoFoto, String tipoFoto, String fechaRegistro, Integer estatus) {
        this.id = id;
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellido = apellido;
        this.telefono = telefono;
        this.celular = celular;
        this.direccion = direccion;
        this.fechaNacimiento = fechaNacimiento;
        this.lugarNacimiento = lugarNacimiento;
        this.email = email;
        this.foto = foto;
        this.nombreFoto = nombreFoto;
        this.tamanoFoto = tamanoFoto;
        this.tipoFoto = tipoFoto;
        this.fechaRegistro = fechaRegistro;
        this.estatus = estatus;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Long getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Long fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getLugarNacimiento() {
        return lugarNacimiento;
    }

    public void setLugarNacimiento(String lugarNacimiento) {
        this.lugarNacimiento = lugarNacimiento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getNombreFoto() {
        return nombreFoto;
    }

    public void setNombreFoto(String nombreFoto) {
        this.nombreFoto = nombreFoto;
    }

    public Long getTamanoFoto() {
        return tamanoFoto;
    }

    public void setTamanoFoto(Long tamanoFoto) {
        this.tamanoFoto = tamanoFoto;
    }

    public String getTipoFoto() {
        return tipoFoto;
    }

    public void setTipoFoto(String tipoFoto) {
        this.tipoFoto = tipoFoto;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getEstatus() {
        return estatus;
    }

    public void setEstatus(Integer estatus) {
        this.estatus = estatus;
    }
}
