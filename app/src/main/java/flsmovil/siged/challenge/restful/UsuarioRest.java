package flsmovil.siged.challenge.restful;

import flsmovil.siged.challenge.modelo.Usuario;
import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;

/**
 * Created by ECarrera on 27/11/14.
 */
public interface UsuarioRest {

    @FormUrlEncoded
    @POST("/usuario/login")
    public void login(@Field("nombreusuario") String username, @Field("passsword") String password, Callback<Usuario> callback);
}
