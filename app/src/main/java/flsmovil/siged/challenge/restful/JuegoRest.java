package flsmovil.siged.challenge.restful;

import java.util.List;

import flsmovil.siged.challenge.modelo.Juego;
import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by ECarrera on 30/11/14.
 */
public interface JuegoRest {

    @GET("/juegos/porJugar")
    public void todos(Callback<List<Juego>> callback);

}
