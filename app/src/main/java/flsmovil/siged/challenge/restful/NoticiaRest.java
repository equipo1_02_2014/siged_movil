package flsmovil.siged.challenge.restful;

import java.util.List;

import flsmovil.siged.challenge.modelo.Noticia;
import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by ECarrera on 06/12/14.
 */
public interface NoticiaRest {

    @GET("/noticia/todas")
    public void todos(Callback<List<Noticia>> callback);
}
