package flsmovil.siged.challenge;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import flsmovil.siged.challenge.Comun.DatosUsuario;
import flsmovil.siged.challenge.modelo.Usuario;
import flsmovil.siged.challenge.restful.UsuarioRest;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class Login extends Activity {

    private EditText txtNombre, txtPassword;
    private Button loginBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtNombre = (EditText)findViewById(R.id.txtNombreusuario);
        txtPassword = (EditText)findViewById(R.id.txtPassword);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void requestLogin(View view) {

        if(!TextUtils.isEmpty(txtNombre.getText().toString()) && !TextUtils.isEmpty(txtPassword.getText().toString())) {
            RestAdapter adapter = new RestAdapter.Builder()
                    .setEndpoint(DatosUsuario.obtenerDatos().URL_API)
                    .build();

            UsuarioRest rest = adapter.create(UsuarioRest.class);

            rest.login(txtNombre.getText().toString(), txtPassword.getText().toString(), new Callback<Usuario>() {
                @Override
                public void success(Usuario usuario, Response response) {

                    if(usuario != null)
                    {
                        // Usuario válido
                        // clasificar inicio de sesion por roles (falta id del rol por parte del servidor, por los momentos solo comparacion por string)
                        // Guardar datos (nombre usuario, rol, imagenes, configuraciones, etc) (falta token de seguridad en parte del servidor)

                        DatosUsuario.obtenerDatos().setId(usuario.getId());
                        DatosUsuario.obtenerDatos().setUsuario(usuario.getNombreusuario());
                        DatosUsuario.obtenerDatos().setRol(usuario.getNombrerol());
                        DatosUsuario.obtenerDatos().setDescripcion(usuario.getDescripcion());
                        DatosUsuario.obtenerDatos().setImagenBytes(usuario.getPhotoBlob());

                        WelcomeDoor(usuario.getNombrerol());
                    } else {
                        Toast.makeText(getApplicationContext(), "Nombre de usuario y/o contraseña incorrectos", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(getApplicationContext(), "Nombre de usuario y/o contraseña vacios", Toast.LENGTH_LONG).show();
        }
    }

    private void WelcomeDoor(String nombrerol)
    {

        if (nombrerol.equals("representante")) {
            Intent representante = new Intent(this, Representante.class);
            finish();
            startActivity(representante);

        } else if (nombrerol.equals("secretaria")) {
            Intent secretaria;
            secretaria = new Intent(this, Secretaria.class);
            finish();
            startActivity(secretaria);

        } else if (nombrerol.equals("entrenador")) {
            Intent entrenador;
            entrenador = new Intent(this, Entrenador.class);
            finish();
            startActivity(entrenador);
        }
    }
}
