package flsmovil.siged.challenge;

import android.app.Activity;
import android.app.Application;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.util.Base64;
import android.widget.TextView;

import com.pkmmte.view.CircularImageView;

/**
 * Created by ECarrera on 27/11/14.
 */
public class Data extends Application{

    private Integer id;
    private String usuario;
    private String imagenBytes;
    private String rol;
    private String descripcion;

    public Data() {
    }

    public Data(Integer id, String usuario, String imagen, String rol, String descripcion) {
        this.id = id;
        this.usuario = usuario;
        this.imagenBytes = imagen;
        this.rol = rol;
        this.descripcion = descripcion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getImagenBytes() {
        return imagenBytes;
    }

    public void setImagenBytes(String imagen) {
        this.imagenBytes = imagen;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BitmapDrawable getImagenPerfil()
    {
        if(!TextUtils.isEmpty(getImagenBytes())) {
            byte[] encodeByte = Base64.decode(this.getImagenBytes(), Base64.DEFAULT);
            return new BitmapDrawable(BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length));
        }
        else return null;
    }

    public void ConfigurarUsuario(Activity activity)
    {
        TextView nombre = (TextView) activity.findViewById(R.id.user_name);
        TextView rol = (TextView) activity.findViewById(R.id.user_role);
        CircularImageView imagen_perfil = (CircularImageView) activity.findViewById(R.id.user_image_profile);

        nombre.setText(getUsuario());
        rol.setText(getRol());
        imagen_perfil.setImageDrawable(getImagenPerfil());
    }
}