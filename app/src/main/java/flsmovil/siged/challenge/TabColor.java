package flsmovil.siged.challenge;

/**
 * Created by ECarrera on 22/10/14.
 */
public class TabColor {
    public static int getColor(int position) {

        switch(position)
        {
            case 0: return 0xFF1A237E; // rojo fls - ActionBar
            case 1: return 0xFF303F9F; // azul cielo
            case 2: return 0xFF0097A7; // verde
            case 3: return 0xFF66BB6A; // naranja
            case 4: return 0xFF43A047; // gris
            case 5: return 0xFF43A047; // azul marino
        }
        return 0xFFC74B46; // rojo fls - ActionBar
    }
}

