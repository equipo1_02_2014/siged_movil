package flsmovil.siged.challenge;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Hildamar on 11/01/2015.
 */
public class Final_Curioso extends Fragment {

    View rootView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.final_curioso, container, false);
        TextView lblTexto= (TextView)rootView.findViewById(R.id.lblIniciaSesion);
        lblTexto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent publica = new Intent(Final_Curioso.this.getActivity(), Principal.class);
                startActivity(publica);
            }
        });
        return rootView;
    }
}
