package flsmovil.siged.challenge.Comun;

import android.app.Activity;
import android.app.Application;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.text.TextUtils;
import android.util.Base64;
import android.widget.TextView;

import com.pkmmte.view.CircularImageView;

import flsmovil.siged.challenge.R;

/**
 * Created by ECarrera on 27/11/14.
 */
public class DatosUsuario extends Application{

    private static DatosUsuario datos = null;

    public static final String URL_API = "http://192.168.1.112:8080/Siged";
    private Integer id;
    private String usuario;
    private String imagenBytes;
    private String rol;
    private String descripcion;

    public DatosUsuario() {}

    // Singleton, variable global de instancia única.
    // creador sincronizado para protegerse de posibles problemas  multi-hilo
    // otra prueba para evitar instanciación múltiple
    final public static void createInstance() {
        if (datos == null) {
            // Sólo se accede a la zona sincronizada
            // cuando la instancia no está creada
            synchronized(DatosUsuario.class) {
                // En la zona sincronizada sería necesario volver
                // a comprobar que no se ha creado la instancia
                if (datos == null) {
                    datos = new DatosUsuario();
                }
            }
        }
    }

    public static DatosUsuario obtenerDatos() {
        if (datos == null) createInstance();
        return datos;
    }

    public Integer getId() {
        return datos.id;
    }

    public void setId(Integer id) {
        this.datos.id = id;
    }

    public String getUsuario() {
        return datos.usuario;
    }

    public void setUsuario(String usuario) {
        this.datos.usuario = usuario;
    }

    public String getImagenBytes() {
        return datos.imagenBytes;
    }

    public void setImagenBytes(String imagenBytes) {
        this.datos.imagenBytes = imagenBytes;
    }

    public String getRol() {
        return datos.rol;
    }

    public void setRol(String rol) {
        this.datos.rol = rol;
    }

    public String getDescripcion() {
        return datos.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.datos.descripcion = descripcion;
    }

    public BitmapDrawable getImagenPerfil()
    {
        if(!TextUtils.isEmpty(getImagenBytes())) {
            byte[] encodeByte = Base64.decode(this.getImagenBytes(), Base64.DEFAULT);
            return new BitmapDrawable(BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length));
        }
        else return null;
    }

    public void ConfigurarUsuario(Activity activity)
    {
        TextView nombre = (TextView) activity.findViewById(R.id.user_name);
        TextView rol = (TextView) activity.findViewById(R.id.user_role);
        CircularImageView imagen_perfil = (CircularImageView) activity.findViewById(R.id.user_image_profile);

        nombre.setText(getUsuario());
        rol.setText(getRol());
        imagen_perfil.setImageDrawable(getImagenPerfil());
    }

    //El método "clone" es sobreescrito por el siguiente que arroja una excepción:
    final public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }
}