package flsmovil.siged.challenge.Comun;

/**
 * Created by ECarrera on 22/10/14.
 */
public class TabColor {
    public static int getColor(int position) {

        switch(position)
        {
            case 0: return 0xFFC74B46; // rojo fls - ActionBar
            case 1: return 0xFF3F9FE0; // azul cielo
            case 2: return 0xFF96AA39; // verde
            case 3: return 0xFFF4842D; // naranja
            case 4: return 0xFF666666; // gris
            case 5: return 0xFF5161BC; // azul marino
        }
        return 0xFFC74B46; // rojo fls - ActionBar
    }
}

