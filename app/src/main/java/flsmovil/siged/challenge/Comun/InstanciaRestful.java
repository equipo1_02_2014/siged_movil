package flsmovil.siged.challenge.Comun;

import retrofit.RestAdapter;

/**
 * Created by ECarrera on 06/12/14.
 */
public class InstanciaRestful {

    private static InstanciaRestful INSTANCE = null;
    private static RestAdapter adapter; // adaptador retrofit

    // Private constructor suppresses
    private InstanciaRestful(){}

    // creador sincronizado para protegerse de posibles problemas  multi-hilo
    // otra prueba para evitar instanciación múltiple
    private static void createInstance() {
        if (INSTANCE == null) {
            // Sólo se accede a la zona sincronizada
            // cuando la instancia no está creada
            synchronized(InstanciaRestful.class) {
                // En la zona sincronizada sería necesario volver
                // a comprobar que no se ha creado la instancia
                if (INSTANCE == null) {
                    INSTANCE = new InstanciaRestful();
                }
            }
        }
    }

    public static InstanciaRestful getInstance() {
        if (INSTANCE == null) createInstance();
        return INSTANCE;
    }

    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    public static RestAdapter RetrofitAdapter()
    {
        if (INSTANCE == null)
        {
            createInstance();
            INSTANCE.adapter = new RestAdapter.Builder()
                        .setEndpoint(DatosUsuario.obtenerDatos().URL_API)
                        .build();
            adapter.setLogLevel(RestAdapter.LogLevel.FULL);
        }
        return INSTANCE.adapter;
    }
}
