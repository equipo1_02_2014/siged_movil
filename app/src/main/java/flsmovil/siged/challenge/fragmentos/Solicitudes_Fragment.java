package flsmovil.siged.challenge.fragmentos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import flsmovil.siged.challenge.R;

/**
 * Created by Hildamar on 25/11/2014.
 */
public class Solicitudes_Fragment extends Fragment {

    View rootView;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.solicitud, container, false);
        return rootView;
    }

}
