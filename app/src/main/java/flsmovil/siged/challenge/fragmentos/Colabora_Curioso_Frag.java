package flsmovil.siged.challenge.fragmentos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import flsmovil.siged.challenge.R;

/**
 * Created by Hildamar on 10/01/2015.
 */
public class Colabora_Curioso_Frag extends Fragment {


    View rootView;

    LinearLayout layout;
    LinearLayout layoutBorrar;
    LinearLayout layoutOpciones;
    ImageView imageBorrar;
    ImageView imageColabora;
    ImageView imageSolicitud;
    LinearLayout layoutColabora;
    LinearLayout layoutSolicitud;
    LinearLayout layoutContenido;
    TextView descripcion;
    Animation animFadein;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.solicitud_curioso, container, false);
        /*layoutColabora = (LinearLayout) rootView.findViewById(R.id.layoutDonacion);
        layoutSolicitud = (LinearLayout) rootView.findViewById(R.id.layoutSolicitud);
        imageColabora = (ImageView) rootView.findViewById(R.id.imageColabora);
        imageSolicitud = (ImageView) rootView.findViewById(R.id.imageSolicitud);
        layout = (LinearLayout) rootView.findViewById(R.id.layautContenedor);
        layoutBorrar = (LinearLayout) rootView.findViewById(R.id.layaoutBorrar);
        layoutOpciones = (LinearLayout) rootView.findViewById(R.id.layoutOpciones);
        imageBorrar = (ImageView) rootView.findViewById(R.id.imageBorrar);*/
        layoutContenido = (LinearLayout) rootView.findViewById(R.id.layoutContenido);
       /* descripcion = (TextView) rootView.findViewById(R.id.lblDescripcion);
        final Context context = this.getActivity();*/
       /* animFadein = AnimationUtils.loadAnimation(context,
                R.anim.fade_in_texto);
        layoutColabora.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        descripcion.setVisibility(View.GONE);
                        layoutContenido.removeAllViews();
                        layoutContenido.setVisibility(View.VISIBLE);
                        layoutOpciones.setVisibility(View.INVISIBLE);
                        //  layoutSolicitud.setVisibility(View.INVISIBLE);
                        layoutBorrar.setVisibility(View.VISIBLE);
                        EditText nombre = new EditText(context);
                        EditText apellido = new EditText(context);
                        EditText telefono = new EditText(context);
                        EditText correo = new EditText(context);
                        Button btnAceptar = new Button(context);
                        Button btnCancelar = new Button(context);
                        LinearLayout contenedorBotones = new LinearLayout(context);
                        contenedorBotones.setOrientation(LinearLayout.HORIZONTAL);
                        contenedorBotones.setGravity(Gravity.CENTER_HORIZONTAL);
                        nombre.setHint("Nombre");
                        apellido.setHint("Apellido");
                        telefono.setHint("Teléfono");
                        correo.setHint("Correo");
                        contenedorBotones.addView(btnAceptar);
                        contenedorBotones.addView(btnCancelar);
                        contenedorBotones.setPadding(0, 10, 0, 0);
                        //
                        layoutContenido.addView(nombre);
                        layoutContenido.addView(apellido);
                        layoutContenido.addView(telefono);
                        layoutContenido.addView(correo);
                        layoutContenido.addView(contenedorBotones);

                        StartAnimations();
                        //layout.addView(layoutContenido);


                    }
                }
        );


        layoutSolicitud.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        descripcion.setVisibility(View.GONE);
                        layoutContenido.removeAllViews();
                        layoutContenido.setVisibility(View.VISIBLE);
                        layoutOpciones.setVisibility(View.INVISIBLE);
                        //  layoutSolicitud.setVisibility(View.INVISIBLE);
                        layoutBorrar.setVisibility(View.VISIBLE);

                        // layoutColabora.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT));
                        //LinearLayout layoutContenedor = new LinearLayout(context);
                        // LinearLayout layoutBorrar = new LinearLayout(context);
                        //  ImageView imageBorrar = new ImageView(context);
                        // imageBorrar.setImageResource(R.drawable.ic_highlight_remove_grey600_48dp);
                        //layoutBorrar.setGravity(Gravity.RIGHT);
                        //  layoutBorrar.addView(imageBorrar);
                        // layoutContenedor.setOrientation(LinearLayout.VERTICAL);
                        EditText nombre = new EditText(context);
                        EditText apellido = new EditText(context);
                        EditText telefono = new EditText(context);
                        EditText correo = new EditText(context);
                        Button btnAceptar = new Button(context);
                        Button btnCancelar = new Button(context);
                        LinearLayout contenedorBotones = new LinearLayout(context);
                        contenedorBotones.setOrientation(LinearLayout.HORIZONTAL);
                        contenedorBotones.setGravity(Gravity.CENTER_HORIZONTAL);
                        nombre.setHint("Nombre");
                        apellido.setHint("Apellido");
                        telefono.setHint("Teléfono");
                        correo.setHint("Correo");
                        contenedorBotones.addView(btnAceptar);
                        contenedorBotones.addView(btnCancelar);
                        contenedorBotones.setPadding(0, 10, 0, 0);

                        layoutContenido.addView(nombre);
                        layoutContenido.addView(apellido);
                        layoutContenido.addView(telefono);
                        layoutContenido.addView(correo);
                        layoutContenido.addView(contenedorBotones);
                        StartAnimations();
                        //layout.addView(layoutContenido);


                    }
                }
        );

        imageBorrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                layoutBorrar.setVisibility(View.INVISIBLE);
                layoutContenido.setVisibility(View.INVISIBLE);
                layoutContenido.removeAllViews();
                //layoutBorrar.removeAllViews();
                layoutColabora.setVisibility(View.VISIBLE);
                layoutSolicitud.setVisibility(View.VISIBLE);
                layoutOpciones.setVisibility(View.VISIBLE);


            }
        });*/

        return rootView;

    }

    private void StartAnimations() {

        Animation anim = AnimationUtils.loadAnimation(this.getActivity(), R.anim.alpha);
        anim.reset();
        layoutBorrar.startAnimation(animFadein);
        layoutContenido.startAnimation(animFadein);



    }
}