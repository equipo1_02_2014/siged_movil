package flsmovil.siged.challenge.fragmentos.calendario;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import flsmovil.siged.challenge.R;

/**
 * Created by ECarrera on 23/10/14.
 */
public class Calendario_Fragment extends Fragment{

    View rootView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragmento_calendario, container, false);
        return rootView;
    }
}
