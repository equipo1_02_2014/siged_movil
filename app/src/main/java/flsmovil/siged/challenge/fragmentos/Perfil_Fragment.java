package flsmovil.siged.challenge.fragmentos;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import flsmovil.siged.challenge.Comun.DatosUsuario;
import flsmovil.siged.challenge.R;


public class Perfil_Fragment extends Fragment {


    private static final int REQUEST_CAMERA = 1888;
    private final int SELECT_FILE = 1;
    View rootView;
    private ImageView imagenUsuario;
    private TextView txtCorreo, txtPass;
    private DatosUsuario datos;

    public static Bitmap getRoundedCornerBitmap(Drawable drawable, boolean square) {
        int width = 0;
        int height = 0;

        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();

        if (square) {
            if (bitmap.getWidth() < bitmap.getHeight()) {
                width = bitmap.getWidth();
                height = bitmap.getWidth();
            } else {
                width = bitmap.getHeight();
                height = bitmap.getHeight();
            }
        } else {
            height = bitmap.getHeight();
            width = bitmap.getWidth();
        }

        Bitmap output = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, width, height);
        final RectF rectF = new RectF(rect);
        final float roundPx = 90;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        datos = new DatosUsuario();
        rootView = inflater.inflate(R.layout.fragmento_perfil, container, false);
        imagenUsuario = (ImageView) rootView.findViewById(R.id.imagenUsuario);
        if (datos.getImagenPerfil() != null)
            imagenUsuario.setImageDrawable(datos.getImagenPerfil());
        txtCorreo = (TextView) rootView.findViewById(R.id.txtCorreo);
        txtPass = (TextView) rootView.findViewById(R.id.txtPassword);

        Button editarImagen = (Button) rootView.findViewById(R.id.btnEditarFoto);
        Button editarCorreo = (Button) rootView.findViewById(R.id.btnEditarCorreo);
        Button editarPass = (Button) rootView.findViewById(R.id.btnEditarPassword);


        //Listener editar imagen
        editarImagen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                selectImage(v);

            }
        });

        //Listener editar correo

        editarCorreo.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        editarCorreo(txtCorreo);

                    }
                }
        );

        //listener editar Contraseña
        editarPass.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        editarPass(txtPass);
                    }
                }
        );

        return rootView;
    }

    public void selectImage(View view) {
        final CharSequence[] items = {"Tomar foto", "Buscar en la galería",
                "Cancelar"};

        AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
        builder.setTitle("Imagen de Reposo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Tomar foto")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(android.os.Environment
                            .getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, REQUEST_CAMERA);
                } else if (items[item].equals("Buscar en la galería")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"),
                            SELECT_FILE);
                } else if (items[item].equals("Cancelar")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == (Activity.RESULT_OK)) {
            if (requestCode == REQUEST_CAMERA) {
                File f = new File(Environment.getExternalStorageDirectory()
                        .toString());
                for (File temp : f.listFiles()) {
                    if (temp.getName().equals("temp.jpg")) {
                        f = temp;
                        break;
                    }
                }
                try {
                    Bitmap bm;
                    BitmapFactory.Options btmapOptions = new BitmapFactory.Options();

                    bm = BitmapFactory.decodeFile(f.getAbsolutePath(),
                            btmapOptions);


                    imagenUsuario.setScaleType(ImageView.ScaleType.FIT_XY);
                    imagenUsuario.setImageBitmap(bm);


                    String path = android.os.Environment
                            .getExternalStorageDirectory()
                            + File.separator
                            + "Phoenix" + File.separator + "default";
                    f.delete();
                    OutputStream fOut = null;
                    File file = new File(path, String.valueOf(System
                            .currentTimeMillis()) + ".jpg");
                    try {
                        fOut = new FileOutputStream(file);
                        bm.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
                        fOut.flush();
                        fOut.close();
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();

                String tempPath = getPath(selectedImageUri, this.getActivity());
                Bitmap bm;
                BitmapFactory.Options btmapOptions = new BitmapFactory.Options();
                bm = BitmapFactory.decodeFile(tempPath, btmapOptions);
                getRoundedCornerBitmap(imagenUsuario.getDrawable(), true);

                imagenUsuario.setScaleType(ImageView.ScaleType.FIT_XY);

                imagenUsuario.setImageBitmap(bm);

            }
        }
    }

    public String getPath(Uri uri, Activity activity) {
        String[] projection = {MediaStore.MediaColumns.DATA};
        Cursor cursor = activity
                .managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private void editarCorreo(final TextView textView) {

        Context context = this.getActivity();
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle("Ingresa tu correo");
        final EditText input = new EditText(context);
        alert.setView(input);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                String srt = input.getEditableText().toString();
                textView.setText(srt);
            }
        });
        alert.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
            }
        });
        AlertDialog alertDialog = alert.create();
        alertDialog.show();


    }

    private void editarPass(final TextView textView) {

        Context context = this.getActivity();

        final AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle("Modificar Contraseña");

        final LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        final EditText passV = new EditText(context);
        passV.setHint("Contraseña Anterior");
        passV.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        passV.setTransformationMethod(PasswordTransformationMethod.getInstance());
        layout.addView(passV);
        final EditText passN = new EditText(context);
        passN.setHint("Contraseña Nueva");
        passN.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        passN.setTransformationMethod(PasswordTransformationMethod.getInstance());
        layout.addView(passN);
        final EditText passN1 = new EditText(context);
        passN1.setHint("Repetir Contraseña");
        passN1.setInputType(InputType.TYPE_TEXT_VARIATION_PASSWORD);
        passN1.setTransformationMethod(PasswordTransformationMethod.getInstance());
        layout.addView(passN1);

        alert.setView(layout);

        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                if (passN.getText() == passN1.getText()) {
                    String srt = passN.getEditableText().toString();

                    textView.setText(srt);
                } else
                    alert.setMessage("Las Claves no coinciden");


                //falta validar la clave anterior con la de inicio de sesion, esperando servicio login

            }
        });
        alert.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                dialog.cancel();
            }
        });
        AlertDialog alertDialog = alert.create();
        alertDialog.show();


    }

}
