package flsmovil.siged.challenge.fragmentos.resultados;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import flsmovil.siged.challenge.R;
import flsmovil.siged.challenge.modelo.Juego;

/**
 * Created by ECarrera on 01/12/14.
 */
public class ResultadosAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater mInflater;
    List<Juego> listaJuegos;

    public ResultadosAdapter(Context context,
                       LayoutInflater inflater) {
        mContext = context;
        mInflater = inflater;
        listaJuegos = new ArrayList<Juego>();

    }

    public void updateData(List<Juego> juegos) {
        // update the adapter's dataset
        listaJuegos.addAll(juegos);
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if(listaJuegos.isEmpty()) {
            return 0;
        }
        else {
            return listaJuegos.size();
        }
    }

    @Override
    public Object getItem(int i) {
        return listaJuegos.get(i);
    }

    @Override
    public long getItemId(int i)
    {
        return listaJuegos.get(i).getId();
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        ViewHolder holder;
        // check if the view already exists
        // if so, no need to inflate and findViewById again!
        if (convertView == null) {
            // Inflate the custom row layout from your XML.
            convertView = mInflater.inflate(R.layout.fila_resultados, null);
            // create a new "Holder" with subviews
            holder = new ViewHolder();
            holder.thumbnailImageView = (ImageView) convertView.findViewById(R.id.imagen_resultados);
            holder.titleTextView = (TextView) convertView.findViewById(R.id.titulo_resultado);
            holder.authorTextView = (TextView) convertView.findViewById(R.id.fecha_resultado);
            // hang onto this holder for future recyclage
            convertView.setTag(holder);
        } else {
            // skip all the expensive inflation/findViewById
            // and just get the holder you already made
            holder = (ViewHolder) convertView.getTag();
        }

        Juego resultado = listaJuegos.get(i);
        String enombre = resultado.getEquipo().getNombre() + " vs " + resultado.getEquipoContrario();
        String cnombre = resultado.getFecha() + " a las " + resultado.getHoraInicio();

        // Send these Strings to the TextViews for display
        holder.titleTextView.setText(String.valueOf(enombre.charAt(0)).toUpperCase() + enombre.substring(1, enombre.length()));
        holder.authorTextView.setText(String.valueOf(cnombre.charAt(0)).toUpperCase() + cnombre.substring(1, cnombre.length()));
        return convertView;
    }

    // this is used so you only ever have to do
    // inflation and finding by ID once ever per View
    private static class ViewHolder {
        public ImageView thumbnailImageView;
        public TextView titleTextView;
        public TextView authorTextView;
    }
}
