package flsmovil.siged.challenge.fragmentos.partidos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import flsmovil.siged.challenge.R;


/**
 * Created by HParra on 23/10/14.
 */
public class ListPartidos_Fragment extends Fragment{
	
	

    View rootView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragmento_listview_partidos, container, false);
        return rootView;
    }
}