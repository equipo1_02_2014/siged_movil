package flsmovil.siged.challenge.fragmentos.noticias;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import flsmovil.siged.challenge.Comun.DatosUsuario;
import flsmovil.siged.challenge.R;
import flsmovil.siged.challenge.modelo.Noticia;
import flsmovil.siged.challenge.restful.NoticiaRest;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class Noticias_Fragment extends Fragment {

    View rootView;
    NoticiasAdapter mNoticiasAdapter;
    ListView listViewNoticias;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragmento_noticias, container, false);

        listViewNoticias = (ListView) rootView.findViewById(R.id.listviewNoticias);
        mNoticiasAdapter = new NoticiasAdapter(getActivity(), inflater);

        listViewNoticias.setAdapter(mNoticiasAdapter);

        listViewNoticias.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Noticia noticia = mNoticiasAdapter.getItem(i);
                Integer id = noticia.getId();
                String titulo = noticia.getTitulo();
                String fecha = noticia.getFecha();
                //String imagen = noticia.getImagen();
                String descripcion = noticia.getDescripcion();
                String enlace = noticia.getEnlace();

                Intent detailIntent = new Intent(getActivity(), Noticia_Detalle.class);
                detailIntent.putExtra("id", id);
                detailIntent.putExtra("titulo", titulo);
                detailIntent.putExtra("fecha", fecha);
                detailIntent.putExtra("descripcion", descripcion);
                detailIntent.putExtra("enlace", enlace);

                startActivity(detailIntent);
            }
        });

        BuscarNoticias();
        return rootView;
    }

    void BuscarNoticias()
    {
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(DatosUsuario.obtenerDatos().URL_API)
                .build();
        adapter.setLogLevel(RestAdapter.LogLevel.FULL); // ver detalles de retrofit en la consola

        NoticiaRest rest = adapter.create(NoticiaRest.class);


        rest.todos(new Callback<List<Noticia>>() {
            @Override
            public void success(List<Noticia> noticias, Response response) {

                if(noticias != null)
                {
                    mNoticiasAdapter.actualizar(noticias);
                    Toast.makeText(getActivity().getApplicationContext(), noticias.get(0).getTitulo(), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void failure(RetrofitError error) {
                if(error != null) {
                    Toast.makeText(getActivity().getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}
