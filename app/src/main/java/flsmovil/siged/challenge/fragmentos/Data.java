package flsmovil.siged.challenge.fragmentos;

final class Data {
  static final String BASE = "http://media.npr.org/assets/";
  static final String EXT = ".jpg";
  static final String[] URLS = {
      BASE + "img/2011/10/24/boys-baseball-2f3c35fa00e75cdb4ddd6f0f276c2c8f33389eaa" + EXT,
      BASE + "img/2014/09/29/la-dodgers-angels_wide-22c0830fea31da13a2685c2b6bb6c425ae3f22b0" + EXT,
      BASE + "img/2014/03/28/miggy282way_custom-89edf272069c5892ece28e3124039b3d98661652" + EXT,
      BASE + "img/2013/02/26/la_022113_fitness_dg_05_15274975-7111dd7988e70aee8a3dfb255100f4f9f29af127-s2-c85" + EXT,
      BASE + "img/2013/10/22/istock_000009539549medium_wide-77927d095889ab62a744a450089c8c980fdc96ba" + EXT,
      BASE + "img/2013/08/13/knucklegrip132way-fca645884fb2f8ae82863db12deeaf6725fc0d2f" + EXT,
      BASE + "img/2014/08/15/whosacitizen-01_slide-b0eb23aaedb2b614c6c9c5dba4aca5f814ab85da-s4-c85" + EXT,
      BASE + "img/2014/10/09/istock_000009310289large-93013079c5572b4bf984b8d85852b6ac7e08d5ba" + EXT,
      BASE + "blogs/health/images/2010/06/baseball-3f36ae524f7ccde97ec47ad84fa20c8031f82526" + EXT,
      BASE + "news/2010/09/09/baseball-d6fd08390c3ecfb8b85b07a866e756601843964c." + EXT
  };

  private Data() {
    // No instances.
  }
}
