package flsmovil.siged.challenge.fragmentos.calendario;



import com.siged.calendar.CalendarFragment;
import com.siged.calendar.CalendarGridAdapter;

public class CalendarSampleCustomFragment extends CalendarFragment {

	@Override
	public CalendarGridAdapter getNewDatesGridAdapter(int month, int year) {
		// TODO Auto-generated method stub
		return new CalendarSampleCustomAdapter(getActivity(), month, year,
				getCaldroidData(), extraData);
	}

}
