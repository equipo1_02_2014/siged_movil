package flsmovil.siged.challenge.fragmentos.calendario;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.siged.calendar.CalendarFragment;
import com.siged.calendar.CalendarListener;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Vector;

import flsmovil.siged.challenge.R;


public class CalendarSampleActivity extends Fragment{
	
	
	//public CalendarFragment caldroidFragment = new CalendarFragment();

	private static final int FILTER = 1;
	private static final int EXIT_ITEM = 2;
	AlertDialog dialogo;
	LinearLayout calendar;
	RadioGroup grupoRadio;
	View rootView;
	ListView listJuegos;
	String fechaString ="2014-11-25";
	int opcion=1;
	final CharSequence[] items = {"Ver Detalle de Partido", "Agregar Partido", "Agregar Recordatorio"};
	private CalendarFragment caldroidFragment;
	private CalendarFragment dialogCaldroidFragment;
	private Vector<Object> datosJuegos = null;
	private Vector<Vector <Object>>juegos=null;
//	ArrayList<Date> numeroCoti = new ArrayList<Integer>();
	
	@SuppressWarnings("deprecation")
	
	private void setearFecha(Date fecha) {
		caldroidFragment.refreshView();
		System.out.println(fechaString);
	Calendar cal = Calendar.getInstance();
	SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
    
    java.sql.Date juego = null;
    
        if (fechaString!=null && fechaString!="")
        {
		juego =java.sql.Date.valueOf(fechaString);
		
		System.out.println("juego"+fecha);
		
        }
        else
        	System.out.println("Fecha"+fechaString);
        	


    //Agregando juego al calendario
	cal=Calendar.getInstance();
	cal.setTime(fecha);
    Date redDate = cal.getTime();
    


		if (caldroidFragment != null) {
			//caldroidFragment.setBackgroundResourceForDate(R.color.blue,
			//		blueDate);
			//caldroidFragment.setBackgroundResourceForDate(R.color.green,
			//		greenDate);
			//caldroidFragment.setTextColorForDate(R.color.white, blueDate);
			//caldroidFragment.setTextColorForDate(R.color.white, greenDate);
			caldroidFragment.setBackgroundResourceForDate(R.color.caldroid_red, redDate);
			caldroidFragment.setTextColorForDate(R.color.white, redDate);
			
		}
		
		
	}

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        rootView = inflater.inflate(R.layout.calendar_main, container, false);
      
        
       caldroidFragment = new CalendarFragment();
        Bundle args = new Bundle();
        Calendar cal = Calendar.getInstance();
        args.putInt(CalendarFragment.MONTH, cal.get(Calendar.MONTH) + 1);
        args.putInt(CalendarFragment.YEAR, cal.get(Calendar.YEAR));
        caldroidFragment.setArguments(args);
       
        
        android.support.v4.app.FragmentTransaction t = getFragmentManager().beginTransaction();
        t.replace(R.id.Calendar1, caldroidFragment);
        t.commit();
        juegos = new Vector<Vector <Object>>();
        datosJuegos = new Vector<Object>();
        boolean exitoP = ListarJuegos(datosJuegos,juegos);
    	boolean exitoC =false;
    	java.sql.Date fechaJuego;
    	
    	if (exitoP){
    		
    		
    		for(int i=0; i<juegos.elementAt(0).size()-1;i++)
    			
    		{
    		Long fecha=(Long)juegos.elementAt(0).elementAt(i);
    		//Long fechaJ=Long.valueOf(fecha);
    		 fechaJuego= new java.sql.Date(fecha);
    		
    		 setearFecha(fechaJuego
    				 );
    		 
    		}
    		
    		
    	}
        
     // Setup listener
		final CalendarListener listener = new CalendarListener() {

						

			@Override
			public void onLongClickDate(Date date, View view) {
				
				onCreateDialog(1);
			}

			@Override
			public void onSelectDate(Date date, View view) {
				// TODO Auto-generated method stub
			//listJuegos=(ListView)view.findViewById(R.id.listaJuegos);
				verJuegos();
				
			
			
				
			}

			
	       
			
			

		};
      
		caldroidFragment.setCaldroidListener(listener);
        
        return rootView;
        
    }
    

  //*************************************************CREANDO EL MENU DE OPCIONES************************************************
  public boolean onCreateOptionsMenu(Menu menu)
  {
      Resources resource = getResources();
     
      menu.add(Menu.NONE, EXIT_ITEM, EXIT_ITEM,resource.getText(R.string.EXIT_ITEM)).setIcon(R.drawable.exit);
      menu.add(Menu.NONE, FILTER, FILTER, resource.getText(R.string.FILTER)).setIcon(R.drawable.filter);
      return true;
  }



  //****************************************SELECCIONANDO OPCIONES DEL MENU*****************************************************
  @Override
  public boolean onOptionsItemSelected(MenuItem item)
  {
      Log.i("[ITEM SELECTED]", "Item " + item.getItemId() + " selected");
      switch (item.getItemId())
      {
      
     
      case FILTER:
      	onCreateDialog(FILTER);
             

          break;
      case EXIT_ITEM:
          dialogo.dismiss();
      }
      return true;
  }


 

//**********************************MOSTRANDO VENTANA DE DIALOGO PARA SELECCIONAR EL TIPO DE FILTRO***************************
  protected Dialog onCreateDialog(int id)
  {
  	
      AlertDialog.Builder builder = new AlertDialog.Builder(this.getActivity());
      builder.setTitle("Seleccione una opción"); //Titulo de la ventana de dialogo
    
      builder.setSingleChoiceItems(items,-1, new DialogInterface.OnClickListener() //Listener para la ventana con Radio Buttons
   
          {

              public void onClick(DialogInterface dialog, int item)
              {
            	  switch (item){ 
            	  	case 0://Ver Deatalle
            		  opcion=1; //Dato para ejecutar el listener
            		break;
            	  	case 1: //Agregar Partido
            	  		opcion =2;//Dato para ejecutar el listener
            	  	break;
            	  	case 2://Agregar Recordatorio
            	  		opcion=3; //Dato para ejecutar el listener
            	  	break;
            	 
            	  }
            	  GestorOpciones(opcion);
            	  dialogo.dismiss(); //Cierra la ventana de dialogo
            	 // cargaMensajes(opcion); //Muestra el mensaje de acuerdo al tipo de filtrado selecionado
              }
             
                 });
            	  dialogo = builder.create(); //Crea la ventana de dialogo
            	  dialogo.show();//Muestra la ventana de dialogo
  				return dialogo; //Retorna el Dialog
          }

	public void GestorOpciones(int opc)
	{
		if(opc ==2)
		{
		agregarJuego();
	//    if(fechaString!=null && fechaString!="")
		//setearFecha();
		}
		
			
	}
	
	private void partidosEquipos()
	{
	 final String[] deportes = {"Los Ratones ", "Los Piratas", "Los Tigres","Panteras","Guaritos","Fanaticos","Pulguitas"};
	 	 AlertDialog.Builder notificacion = new AlertDialog.Builder(this.getActivity());
	 
	 notificacion.setTitle("Selecciona un elemento");
	 notificacion.setItems(deportes, new DialogInterface.OnClickListener() {
	 public void onClick(DialogInterface dialog, int posicion) {
	 Log.i("Dialogos", "Opción elegida: " + deportes[posicion]);
	 }});
	 notificacion.show();
	}




private void agregarJuego()
{
	
	Context context=this.getActivity();
	       /* Alert Dialog Code Start*/     
	            AlertDialog.Builder alert = new AlertDialog.Builder(context);
	            alert.setTitle("Indique la Fecha"); //Set Alert dialog title here
	            alert.setMessage("Fecha Aquí"); //Message here
	 
	            // Set an EditText view to get user input 
	            final EditText input = new EditText(context);
	            alert.setView(input);
	 
	            alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int whichButton) {
	             //You will get as string input data in this variable.
	             // here we convert the input to a string and show in a toast.
	             String srt = input.getEditableText().toString();
	             fechaString=srt;
	                         
	            } 
	        }); //End of alert.setPositiveButton
	            alert.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
	              public void onClick(DialogInterface dialog, int whichButton) {
	                // Canceled.
	                  dialog.cancel();
	              }
	        }); //End of alert.setNegativeButton
	            AlertDialog alertDialog = alert.create();
	            alertDialog.show();
	            
	            
	            /* Alert Dialog Code End*/        
	     }// End of onClick(View v)


private void verJuegos()
{
	ArrayList<Object>equipoContrario = new ArrayList<Object>();
	ArrayList<Object>horaInicio =  new ArrayList<Object>();;
	ArrayList<Object>lugar =  new ArrayList<Object>();
	Date fechaJuego;
	ArrayList<ArrayList<Object>>datos = new ArrayList<ArrayList<Object>>();
	for(int j=1;j<=3;j++)
	for(int i=0; i<juegos.elementAt(j).size()-1;i++)
	{
		if(j==1)
		equipoContrario.add(i,juegos.elementAt(1).elementAt(i).toString());
		if(j==3)
		lugar.add(i,juegos.elementAt(3).elementAt(i).toString());
		if(j==2)
		{
		Long fecha=(Long)juegos.elementAt(2).elementAt(i);
		//Long fechaJ=Long.valueOf(fecha);
		 fechaJuego= new java.sql.Date(fecha);
		 horaInicio.add(i,fechaJuego.getTime());
		}
		
	}
	datos.add(0,equipoContrario);
	datos.add(1,horaInicio);
	datos.add(2,lugar);
	
	
	Context context=this.getActivity();
	       /* Alert Dialog Code Start*/     
	            AlertDialog.Builder alert = new AlertDialog.Builder(context);
	            alert.setTitle("Indique la Fecha"); //Set Alert dialog title here
	          //  alert.setMessage("Fecha Aquí"); //Message here
	            
	            ArrayList<String> datos1= new ArrayList<String>();
	            for(int i=0; i<juegos.get(1).size();i++)
	            	datos1.add((String) juegos.elementAt(1).get(i));

	            final CharSequence[] itemsEquiposContrarios=datos1.toArray(new CharSequence[datos1.size()]);
	           // final CharSequence[] itemsLugar=juegos.toArray(new CharSequence[juegos.elementAt(3).size()]);
	            
	           alert.setItems(itemsEquiposContrarios, new DialogInterface.OnClickListener(){
	        	   public void onClick(DialogInterface dialog, int item){
	        	   System.out.println("PAso");}});
	      //     }
	    //       });
	           
	            
	            // Set an EditText view to get user input 
	        //    final ListView input = new ListView(context);
	      //      input.
	            //alert.setView(input);
	 
	           // alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
	         //   public void onClick(DialogInterface dialog, int whichButton) {
	             //You will get as string input data in this variable.
	             // here we convert the input to a string and show in a toast.
	           //  String srt = input.getEditableText().toString();
	             //fechaString=srt;
	                         
	          //End of alert.setPositiveButton
	          //  alert.setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
	            //  public void onClick(DialogInterface dialog, int whichButton) {
	                // Canceled.
	              //    dialog.cancel();
	            //  }
	        //}); //End of alert.setNegativeButton
	            AlertDialog alertDialog = alert.create();
	            alertDialog.show();
	            
	            
	            /* Alert Dialog Code End*/        
	     }// End of onClick(View v)

public View getView(final int position, View vista, ViewGroup parent) {
	
    final ViewHolder holder =new ViewHolder();
    
    holder.equipoContrario.setText(juegos.elementAt(1).elementAt(position).toString());
    holder.horaInicio.setText(juegos.elementAt(2).elementAt(position).toString());
    holder.lugar.setText(juegos.elementAt(3).elementAt(position).toString());
    
    vista.setTag(holder);
    return vista;
    
    
    
}

public class ViewHolder {
    
    TextView equipoContrario;
    TextView horaInicio;
    TextView lugar;
    
   
 
}
private boolean ListarJuegos(java.util.Vector<Object> datosJuegos, java.util.Vector<Vector <Object>>juegos){


	   boolean exito = false;
	   HttpClient httpclient = new DefaultHttpClient();
	 
	   
	// Preparar un objeto request via method Get
	   HttpGet httpget = new HttpGet("http://172.19.24.36:8080/Siged/juego/todos");
     HttpResponse response;
   
     try {
  	  
  	   response = httpclient.execute(httpget); // Ejecutar el request	        	   
        HttpEntity entity = response.getEntity();// Obtener la entidad del response
         // Si el response no esta encerrado como una entity, no hay necesidad de preocuparse, liberar la conexion
         if (entity != null) {	                  
      	   String resultado = EntityUtils.toString(response.getEntity()); // el JSON Response es leido
 
      	   JSONObject json =null; 
      	   try{
      	  JSONArray jsonA=new JSONArray(resultado); 
          Vector<Object> fechaJuegos=new Vector<Object>();
        Vector<Object> equiposContrarios=new Vector<Object>();
        Vector<Object> horaInicio=new Vector<Object>();
        Vector<Object> lugar=new Vector<Object>();
        for(int i=0;i<jsonA.length();i++){
        	JSONObject jsonJ =jsonA.getJSONObject(i);
        	
        
        	fechaJuegos.add((Long)jsonJ.get("fecha"));    
            equiposContrarios.add((String)jsonJ.get("equipocontrario"));
        	horaInicio.add((Long)jsonJ.get("horainicio"));
        	lugar.add((String)jsonJ.get("lugar"));
        	
        	
      	   }
        juegos.add(0,fechaJuegos); //0
        juegos.add(1,equiposContrarios);//1
        juegos.add(2,horaInicio);//2
        juegos.add(3,lugar);//3
        
      	   }
      	   catch(JSONException e){
      		   System.out.println(e.toString());
      	   }
      		   
      	   }
 
             System.out.println("Aquii  "+juegos.get(0));

             System.out.println("Prove  "+juegos.elementAt(1));

             exito = true;
        
         }
     
     catch (Exception e) {
    	 System.out.println(e);
    
     }
	   return exito;

}

public Object getChild(int groupPosition, int childPosition) {
	Vector<Object>datos= new Vector<Object>();
	datos=juegos.elementAt(groupPosition);
	//  ArrayList<Object> listaJuegos =  (ArrayList<Object>) juegos.get(groupPosition).get(childPosition);
	  return datos.get(childPosition);
	 }
	 
	 public long getChildId(int groupPosition, int childPosition) {
	  return childPosition;
	 }
	 
	 
	 
	 
}





