package flsmovil.siged.challenge.fragmentos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.ToggleButton;

import com.squareup.picasso.Picasso;

import flsmovil.siged.challenge.R;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class Noticias_Fragment extends Fragment {

    View rootView;
    private ToggleButton showHide;
    private FrameLayout sampleContent;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragmento_noticias, container, false);
        sampleContent = (FrameLayout) rootView.findViewById(R.id.sample_content);

        final ListView activityList = (ListView) rootView.findViewById(R.id.activity_list);
      /*  final PicassoSampleAdapter adapter = new PicassoSampleAdapter(this.getActivity());
        activityList.setAdapter(adapter);
        activityList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                adapter.getItem(position).launch(Noticias_Fragment.this);
            }
        });
*/
        showHide = (ToggleButton) rootView.findViewById(R.id.faux_action_bar_control);
        showHide.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                activityList.setVisibility(checked ? VISIBLE : GONE);
            }
        });

        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //Picasso.with(this.getActivity()).cancelTag(this.getActivity());
    }

    public void onBackPressed() {
        if (showHide.isChecked()) {
            showHide.setChecked(false);
        } else {
            super.getActivity().onBackPressed();
        }
    }

    public void setContentView(int layoutResID) {
        this.getActivity().getLayoutInflater().inflate(layoutResID, sampleContent);
    }

    public void setContentView(View view) {
        sampleContent.addView(view);
    }

    public void setContentView(View view, ViewGroup.LayoutParams params) {
        sampleContent.addView(view, params);
    }
}





    /*

        @Override protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            super.setContentView(R.layout.fragmento_noticias);
            sampleContent = (FrameLayout) findViewById(R.id.sample_content);

            final ListView activityList = (ListView) findViewById(R.id.activity_list);
            final PicassoSampleAdapter adapter = new PicassoSampleAdapter(this);
            activityList.setAdapter(adapter);
            activityList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    adapter.getItem(position).launch(PicassoSampleActivity.this);
                }
            });

            showHide = (ToggleButton) findViewById(R.id.faux_action_bar_control);
            showHide.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {
                    activityList.setVisibility(checked ? VISIBLE : GONE);
                }
            });
        }

        @Override
        protected void onDestroy() {
            super.onDestroy();
            Picasso.with(this).cancelTag(this);
        }

        @Override public void onBackPressed() {
            if (showHide.isChecked()) {
                showHide.setChecked(false);
            } else {
                super.onBackPressed();
            }
        }

        @Override public void setContentView(int layoutResID) {
            getLayoutInflater().inflate(layoutResID, sampleContent);
        }

        @Override public void setContentView(View view) {
            sampleContent.addView(view);
        }

        @Override public void setContentView(View view, ViewGroup.LayoutParams params) {
            sampleContent.addView(view, params);
        }
    }
}*/
