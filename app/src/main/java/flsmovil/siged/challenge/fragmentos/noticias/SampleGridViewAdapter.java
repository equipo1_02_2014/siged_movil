package flsmovil.siged.challenge.fragmentos.noticias;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import flsmovil.siged.challenge.R;

import static android.widget.ImageView.ScaleType.CENTER_CROP;

final class SampleGridViewAdapter extends BaseAdapter {
  private Context context;
  private  String[] noticias;
    private int[] imagenes;
  private  ArrayList<String> rutas;



  private final List<String> urls = new ArrayList<String>();

  public SampleGridViewAdapter(Context context, String[] noticias, int imagenes[]) {
    this.context = context;
      this.noticias=noticias;
      this.imagenes = imagenes;

    // Ensure we get a different ordering of images on each run.
    Collections.addAll(urls, Data.URLS);
    Collections.shuffle(urls);


    // Triple up the list.
    ArrayList<String> copy = new ArrayList<String>(urls);
    urls.addAll(copy);
    urls.addAll(copy);
  }

  @Override public View getView(int position, View convertView, ViewGroup parent) {
   //  SquaredImageView view = (SquaredImageView) convertView;
      View grid;
     // grid = new View(context);

      LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      TextView tv;
      ImageView imageView;

    if (convertView == null) {
      //  view = new SquaredImageView(context);
        grid = new View(context);
        grid = inflater.inflate(R.layout.noticias_item, null);
        TextView textView = (TextView) grid.findViewById(R.id.textImagen);
        imageView = (ImageView) grid.findViewById(R.id.imagenNoti);
        if(position<=8) {
            textView.setText(noticias[position]);
            imageView.setImageResource(imagenes[position]);
        }


        //grid.setPadding(50,50,50,50);
        //imageView.setCropToPadding(true);
        //imageView.setAdjustViewBounds(true);
        imageView.setScaleType(CENTER_CROP);




    }
        else
        grid = (View) convertView;
       // imageView.setImageURI();

     // view.setScaleType(MATRIX);
      //  view.animate();



    // Get the image URL for the current position.
    String url = getItem(position);

    // Trigger the download of the URL asynchronously into the image view.
   // Picasso.with(context) //
     //   .load(url) //


       // .placeholder(R.drawable.placeholder) //

       // .error(R.drawable.error) //

//        .fit(); //
        //.tag(context) //acomodas
       // .into(imageView);


    return grid;
  }




  @Override public int getCount() {
    return urls.size();
  }

  @Override public String getItem(int position) {
    return urls.get(position);
  }

  @Override public long getItemId(int position) {
    return position;
  }
}

final class Data {
    static final String BASE = "http://i.imgur.com/";
    static final String EXT = ".jpg";
    static final String[] URLS = {
            BASE + "CqmBjo5" + EXT, BASE + "zkaAooq" + EXT, BASE + "0gqnEaY" + EXT,
            BASE + "9gbQ7YR" + EXT, BASE + "aFhEEby" + EXT, BASE + "0E2tgV7" + EXT,
            BASE + "P5JLfjk" + EXT, BASE + "nz67a4F" + EXT, BASE + "dFH34N5" + EXT,
            BASE + "FI49ftb" + EXT, BASE + "DvpvklR" + EXT, BASE + "DNKnbG8" + EXT,
            BASE + "yAdbrLp" + EXT, BASE + "55w5Km7" + EXT, BASE + "NIwNTMR" + EXT,
            BASE + "DAl0KB8" + EXT, BASE + "xZLIYFV" + EXT, BASE + "HvTyeh3" + EXT,
            BASE + "Ig9oHCM" + EXT, BASE + "7GUv9qa" + EXT, BASE + "i5vXmXp" + EXT,
            BASE + "glyvuXg" + EXT, BASE + "u6JF6JZ" + EXT, BASE + "ExwR7ap" + EXT,
            BASE + "Q54zMKT" + EXT, BASE + "9t6hLbm" + EXT, BASE + "F8n3Ic6" + EXT,
            BASE + "P5ZRSvT" + EXT, BASE + "jbemFzr" + EXT, BASE + "8B7haIK" + EXT,
            BASE + "aSeTYQr" + EXT, BASE + "OKvWoTh" + EXT, BASE + "zD3gT4Z" + EXT,
            BASE + "z77CaIt" + EXT,
    };
    private Data() {
// No instances.
    }
}
