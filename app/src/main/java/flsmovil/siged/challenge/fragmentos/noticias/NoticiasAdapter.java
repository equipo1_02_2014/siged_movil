package flsmovil.siged.challenge.fragmentos.noticias;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import flsmovil.siged.challenge.R;
import flsmovil.siged.challenge.modelo.Noticia;

/**
 * Created by ECarrera on 06/12/14.
 */
public class NoticiasAdapter extends BaseAdapter {

    Context mContext;
    LayoutInflater mInflater;
    List<Noticia> listaNoticias;

    public NoticiasAdapter(Context context, LayoutInflater inflater)
    {
        mContext = context;
        mInflater = inflater;
        listaNoticias = new ArrayList<Noticia>();
    }

    public void actualizar(List<Noticia> noticias)
    {
        listaNoticias.addAll(noticias);
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        if(listaNoticias.isEmpty()) {
            return 0;
        }
        else {
            return listaNoticias.size();
        }
    }

    @Override
    public Noticia getItem(int i) {
        return listaNoticias.get(i);
    }

    @Override
    public long getItemId(int i){ return listaNoticias.get(i).getId(); }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {

        ViewHolder holder;
        // check if the view already exists
        // if so, no need to inflate and findViewById again!
        if (convertView == null) {
            // Inflate the custom row layout from your XML.
            convertView = mInflater.inflate(R.layout.fila_noticia, null);
            // create a new "Holder" with subviews
            holder = new ViewHolder();
            holder.thumbnailImageView = (ImageView) convertView.findViewById(R.id.imagen_noticia);
            holder.titleTextView = (TextView) convertView.findViewById(R.id.titulo_noticia);
            holder.authorTextView = (TextView) convertView.findViewById(R.id.fecha_noticia);
            // hang onto this holder for future recyclage
            convertView.setTag(holder);
        } else {
            // skip all the expensive inflation/findViewById
            // and just get the holder you already made
            holder = (ViewHolder) convertView.getTag();
        }

        Noticia noticia = listaNoticias.get(i);
        String titulo = noticia.getTitulo();
        String descripcion = noticia.getFecha();

        // Send these Strings to the TextViews for display
        holder.thumbnailImageView.setImageDrawable(mContext.getResources().getDrawable(R.drawable.image1));
        holder.titleTextView.setText(titulo);
        holder.authorTextView.setText(descripcion);
        return convertView;
    }

    // this is used so you only ever have to do
    // inflation and finding by ID once ever per View
    private static class ViewHolder {
        public ImageView thumbnailImageView;
        public TextView titleTextView;
        public TextView authorTextView;
    }
}
