package flsmovil.siged.challenge.fragmentos.resultados;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import java.util.List;

import flsmovil.siged.challenge.Comun.DatosUsuario;
import flsmovil.siged.challenge.R;
import flsmovil.siged.challenge.modelo.Juego;
import flsmovil.siged.challenge.restful.JuegoRest;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ECarrera on 23/10/14.
 */
public class Resultados_Fragment extends Fragment {

    View rootView;
    ResultadosAdapter mResultadosAdapter;
    ListView listViewResultados;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragmento_resultados, container, false);

        listViewResultados = (ListView) rootView.findViewById(R.id.listviewResultados);
        mResultadosAdapter = new ResultadosAdapter(getActivity(), inflater);

        listViewResultados.setAdapter(mResultadosAdapter);

        BuscarJuegos();
        return rootView;
    }

    void BuscarJuegos()
    {
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(DatosUsuario.obtenerDatos().URL_API)
                .build();
        adapter.setLogLevel(RestAdapter.LogLevel.FULL); // ver detalles de retrofit en la consola

        JuegoRest rest = adapter.create(JuegoRest.class);

        rest.todos(new Callback<List<Juego>>() {
            @Override
            public void success(List<Juego> juegos, Response response) {

                if(juegos != null)
                {
                    mResultadosAdapter.updateData(juegos);
                    Toast.makeText(getActivity(),juegos.get(0).getEquipo().getNombre()+ " " +juegos.get(0).getEquipo().getCategoria().getNombre(),Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if(error!= null) {
                    Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }
}