package flsmovil.siged.challenge.fragmentos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import flsmovil.siged.challenge.R;

/**
 * Created by ECarrera on 23/10/14.
 */
public class Resultados_Fragment extends Fragment{

    View rootView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.resultados_juegos, container, false);
        return rootView;
    }
}