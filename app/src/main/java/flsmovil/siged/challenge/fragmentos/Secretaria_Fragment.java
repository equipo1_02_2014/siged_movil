package flsmovil.siged.challenge.fragmentos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import flsmovil.siged.challenge.R;

/**
 * Created by ECarrera on 27/11/14.
 */
public class Secretaria_Fragment extends Fragment {

    View rootView;
    private ImageView drawer_icon;
    private DrawerLayout mDrawerLayout;
    private LinearLayout mDrawerContainer;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.secretaria, container, false);

        drawer_icon = (ImageView) rootView.findViewById(R.id.drawer_icon);

        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        mDrawerContainer = (LinearLayout) getActivity().findViewById(R.id.drawer_container);

        drawer_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.openDrawer(mDrawerContainer);
            }
        });

        return rootView;
    }
}
