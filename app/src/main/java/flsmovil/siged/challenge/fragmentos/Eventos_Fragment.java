package flsmovil.siged.challenge.fragmentos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Arrays;

import flsmovil.siged.challenge.R;

public class Eventos_Fragment extends Fragment {

	View rootView;
   private ListView mainListView ;
    private ArrayAdapter<String> listAdapter ;


    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.eventos_fragment, container, false);
        mainListView =(ListView)rootView.findViewById(R.id.ListView_listado);




        // Create and populate a List of planet names.
        String [] evento = new String [] {"Fecha", "Direccion", "Observacion",
                "Nombre", "Hora", "Responsable", "Contacto Telefonico", "Contacto Correo",
                "Patrocinante", "Fecha registro"};
        ArrayList<String> planetList = new ArrayList<String>();
        planetList.addAll( Arrays.asList(evento) );

        // Create ArrayAdapter using the planet list.
        listAdapter = new ArrayAdapter<String>(this.getActivity(), R.layout.listasevento, planetList);

        // Add more planets. If you passed a String[] instead of a List<String>
        // into the ArrayAdapter constructor, you must not add more items.
        // Otherwise an exception will occur.
        listAdapter.add( "30-11-14" );
        listAdapter.add( "frente a la plaza bolivar" );
        listAdapter.add( "le gusta cantar" );
        listAdapter.add( "venezuela" );
        listAdapter.add( "5:30" );
        listAdapter.add( "Eutimio Rivas" );
        listAdapter.add( "04162348974" );
        listAdapter.add( "evento@fundacionluisojos.com" );
        listAdapter.add( "Cardenales" );
        listAdapter.add( "10-11-14" );


        // Set the ArrayAdapter as the ListView's adapter.
        mainListView.setAdapter( listAdapter );

        return rootView;
    }

}
