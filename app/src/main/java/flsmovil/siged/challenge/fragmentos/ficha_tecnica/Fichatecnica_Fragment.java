package flsmovil.siged.challenge.fragmentos.ficha_tecnica;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import flsmovil.siged.challenge.R;

/**
 * Created by mariale on 10/11/14.
 */
public class Fichatecnica_Fragment extends Fragment {

        View rootView;

        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            rootView = inflater.inflate(R.layout.ficha_tecnica_jugador, container, false);

            return rootView;
        }

}
