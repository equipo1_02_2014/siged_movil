package flsmovil.siged.challenge.fragmentos;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;

import flsmovil.siged.challenge.R;

/**
 * Created by ECarrera on 23/10/14.
 */
public class Principal_Fragment extends Fragment {

    View rootView;
    private ImageView drawer_icon;
    private DrawerLayout mDrawerLayout;
    private LinearLayout mDrawerContainer;
    private WebView flash;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.principal, container, false);

       /*
        flash = (WebView)rootView.findViewById(R.id.flashAndroid);
        flash.getSettings().setJavaScriptEnabled(true);
        flash.getSettings().setPluginsEnabled(true);
        flash.getSettings().setAllowContentAccess(true);
        flash.loadUrl("file:///android_asset/fls.swf");*/
        drawer_icon = (ImageView) rootView.findViewById(R.id.drawer_icon);

        mDrawerLayout = (DrawerLayout) getActivity().findViewById(R.id.drawer_layout);
        mDrawerContainer = (LinearLayout) getActivity().findViewById(R.id.drawer_container);

        drawer_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mDrawerLayout.openDrawer(mDrawerContainer);
            }
        });

        return rootView;
    }
}