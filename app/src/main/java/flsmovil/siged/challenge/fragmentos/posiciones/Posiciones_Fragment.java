package flsmovil.siged.challenge.fragmentos.posiciones;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import flsmovil.siged.challenge.R;
/**
 * Created by ECarrera on 23/10/14.
 */
public class Posiciones_Fragment extends Fragment {

    View rootView;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.tabla_posiciones, container, false);
        return rootView;
    }
}


