package flsmovil.siged.challenge;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

/**
 * Created by Hildamar on 28/12/2014.
 */


public class InicioFlash extends Activity {

    ImageView imageView;
    Animation animFadein;
    ProgressBar progressInicio;
    private int progressStatus = 0;
    private static int SPLASH_TIME_OUT = 4300;
    private Handler handler = new Handler();
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        Window window = getWindow();
        window.setFormat(PixelFormat.RGBA_8888);
    }
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inicio);
         imageView = (ImageView)findViewById(R.id.imageLogo);
         progressInicio= (ProgressBar)findViewById(R.id.progressBarInicio);
         animFadein = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.fade_in);
        StartAnimations();

        new Thread(new Runnable() {
            public void run() {

                while (progressStatus < 100) {
                    progressStatus += 1;


                    handler.post(new Runnable() {
                        public void run() {
                            progressInicio.setProgress(progressStatus);
                           // Intent yes_krao = new Intent(InicioFlash.this, Principal.class);
                            //startActivity(yes_krao);
                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        //Just to display the progress slowly
                        Thread.sleep(50);


                            // This method will be executed once the timer is over
                            // Start your app main activity


                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
        new Handler().postDelayed(new Runnable() {

           /* *//*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent yes_krao = new Intent(InicioFlash.this, PrincipalCurioso.class);
                startActivity(yes_krao);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    private void StartAnimations() {
        getActionBar().hide();
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.alpha);
        anim.reset();
        LinearLayout l=(LinearLayout) findViewById(R.id.lin_lay);
        l.clearAnimation();
        l.startAnimation(anim);
        imageView.setVisibility(View.VISIBLE);
        imageView.startAnimation(animFadein);
        progressInicio.setVisibility(View.VISIBLE);
        progressInicio.startAnimation(animFadein);


        anim = AnimationUtils.loadAnimation(this, R.anim.translate);
        anim.reset();
        ImageView iv = (ImageView) findViewById(R.id.logo);
        iv.clearAnimation();
        iv.startAnimation(anim);



    }

}
/*public class InicioFlash extends Activity {

    private static int SPLASH_TIME_OUT = 10000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.inicio);
      //  ImageView imageView = (ImageView) findViewById(R.id.flashView);


      /*  int i;
        for (i = 1; i <= 26; i++) {
            imageView.startAnimation(selectAnimation(i));

        }
    }

    private Animation selectAnimation(int index)
    {
      switch (index)
      {
          case 0:

          }
      }

    }



        TransitionDrawable transition= (TransitionDrawable)getResources().getDrawable(R.drawable.animation_init);

        transition.startTransition(1000);

        ImageView imageView = (ImageView)findViewById(R.id.flashView);
        imageView.setImageDrawable(transition);

       String localUrl ="file:///android_asset/www/fls.html";

        Context mContext= InicioFlash.this;

        WebView wv=new WebView(mContext);
         wv=(WebView) findViewById(R.id.flash);
        wv.getSettings().setJavaScriptEnabled(true);
        wv.getSettings().setPluginsEnabled(true);
        wv.getSettings().setPluginState(WebSettings.PluginState.ON);
        wv.getSettings().setAllowFileAccess(true);


        wv.loadUrl(localUrl);
       // setContentView(wv);



        new Handler().postDelayed(new Runnable() {

           /* *//*
             * Showing splash screen with a timer. This will be useful when you
             * want to show case your app logo / company
             *//*/

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent yes_krao = new Intent(InicioFlash.this, Principal.class);
                startActivity(yes_krao);
                finish();
            }
        }, SPLASH_TIME_OUT);
    }
}*/
