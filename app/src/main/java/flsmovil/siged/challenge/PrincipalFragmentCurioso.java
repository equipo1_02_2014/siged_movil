package flsmovil.siged.challenge;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Hildamar on 11/01/2015.
 */


public class PrincipalFragmentCurioso extends Fragment {

    View rootView;
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.principal_curioso, container, false);
        return rootView;

    }
}
